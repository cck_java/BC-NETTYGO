package nafos.core.annotation.controller;

import nafos.core.annotation.rpc.RemoteCall;

@RemoteCall
@Controller
public @interface RemoteController {
}
