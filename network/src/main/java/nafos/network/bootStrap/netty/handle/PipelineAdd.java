package nafos.network.bootStrap.netty.handle;

import io.netty.channel.ChannelPipeline;

public interface PipelineAdd {

    void handAdd(ChannelPipeline pipeline);
}
